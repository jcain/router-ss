<?php
use \JCain\Router\SS\Handler;
use \JCain\Router\SS\Request;
use \JCain\Router\SS\Requests\StringEvaluator;


class RequestHandler implements Handler {
	private $phrase;


	public function __construct(string $phrase) {
		$this->phrase = $phrase;
	}


	public function handle(Request $r) : void {
		$path = new StringEvaluator($r, 'path');

		$path->equals('', function ($r) {
			echo "Example #4: Named class handler, multiple files<br />\n";
			echo "<b>$this->phrase</b>";
		});
	}
}