<?php
require_once __DIR__ . '/vendor/autoload.php';

use \JCain\Router\SS\Handlers\PhpFileHandler;
use \JCain\Router\SS\Requests\BasicRequest;
use \JCain\Router\SS\Requests\StringEvaluator;
use \JCain\Router\SS\Routers\BasicRouter;

$request = new BasicRequest([
	'variables' => BasicRequest::parseRequestInfo(),
]);

$router = new BasicRouter([ 'outputExceptions' => true ]);
$router->route($request, function ($r) {
	$path = new StringEvaluator($r, 'path');

	$path->equals('', function ($r) {
		echo '<a href="hello-world">Click here to greet the world<a><br />';
		echo 'or<br />';
		echo '<a href="hello-universe">Click here to greet the universe<a><br />';

		echo '<hr />';
		echo '<a href="example1/">Example #1</a> An anonymous function as the handler.<br />';
		echo '<a href="example2/">Example #2</a> An anonymous class as the handler.<br />';
		echo '<a href="example3/">Example #3</a> A named class as the handler.<br />';
	});

	$path->matches('^hello-(?<name>[^/]+)$', function ($r) {
		$name = ucwords(urldecode($r->getValue('name')));
		echo "Hello, $name!";
	});

	$path->matches('^example(\d+)/', function ($r) {
		$subfolder = $r->getValue(0);
		$handler = new PhpFileHandler([
			'file' => __DIR__ . "/{$subfolder}router.php",
		]);
		$handler->handle($r);
	});
});