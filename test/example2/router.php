<?php
use \JCain\Router\SS\Handler;
use \JCain\Router\SS\Request;
use \JCain\Router\SS\Requests\StringEvaluator;

$request->evaluate(new class implements Handler {
	public function handle(Request $r) : void {
		$path = new StringEvaluator($r, 'path');

		$path->equals('', function ($r) {
			echo "Example #3: Anonymous class handler, single-file<br />\n";
		});
	}
});