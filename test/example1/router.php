<?php
use \JCain\Router\SS\HttpStatusException;
use \JCain\Router\SS\Handlers\RawFileHandler;
use \JCain\Router\SS\Requests\StringEvaluator;

$request->evaluate(function ($r) {
	if ($r->getValue('method') !== 'GET')
		throw new HttpStatusException(405);

	$path = new StringEvaluator($r, 'path');

	$path->equals('', function ($r) {
		echo 'Example #1: Anonymous function handler, single-file<br />';
		echo '<a href="hello-world">A plaintext greeting</a><br />';
		echo '<a href="empty-success">Empty but successful</a> (Loads a blank page)<br />';
		echo '<a href="non-empty-failure">Not empty and failure</a> (Loads a 404 page)<br />';
		echo '<a href="recursion-failure">Recursion failure</a> (Loads a 500 page)<br />';
		echo '<br />';
		echo '<form action="." method="post"><input type="submit" /></form>';
		echo '<hr />';
		echo '<img src="nyancat.gif" /><br />';
	});

	$path->equals('hello-world', function ($r) {
		header('Content-Type: text/plain; charset=utf-8');
		echo 'Hello, World!';
	});

	$path->matches('add-(\d+)-plus-(\d+)', function ($r) {
		$a = $r->getValue(1);
		$b = $r->getValue(2);
		echo "$a + $b = " . ($a + $b);
	});

	$path->equals('empty-success', function ($r) {
		// This succeeds because the Request is not evaluated.
	});

	$path->equals('non-empty-failure', function ($r) {
		// This fails because the Request is evaluated but did not result in being handled.
		$r->evaluate($r->fallthrough());
	});

	$path->equals('resursion-failure', function ($r) {
		// This fails because the Request is evaluated while being evaluated.
		$r->evaluate(function () use ($r) {
			$r->evaluate(null);
		});
	});

	$path->equals('nyancat.gif', function ($r) {
		$handler = new RawFileHandler([
			'file' => __DIR__ . '/nyancat.gif',
			'type' => 'image/gif',
		]);
		$handler->handle($r);
	});
});