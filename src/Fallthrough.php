<?php namespace JCain\Router\SS;


/// Stability: alpha, Since: 0.9
final class Fallthrough{
	static private $instance;


	private function __construct() {
		// Do nothing.
	}


	public function __set($name, $value) {
		throw new \LogicException('Cannot set properties on this object');
	}


	public function __clone() {
		throw new \LogicException('Cannot clone this object');
	}


	public function __toString() {
		return 'FALLTHROUGH';
	}


	static public function instance() {
		if (!self::$instance) {
			self::$instance = new Fallthrough();
		}
		return self::$instance;
	}
}