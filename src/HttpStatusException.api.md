# JCain\Router\SS\HttpStatusException


## HttpStatusException class

_Stability: **alpha**, Since: **0.0**_

```php
class HttpStatusException extends Exception {
  // Constructor
  public function __construct($code : int, string $message : string = null)
}
```


### Constructor


#### __construct()

```php
public function __construct($code : int, $message : string = null)
```