# JCain\Router\SS\Fallthrough


## Fallthrough class

_Stability: **alpha**, Since: **0.9**_

Used as a returned instruction to fall through to the next [`Request`](Request.api.md) evaluation.

```php
final class Fallthrough {
  // Static Methods
  static public function instance() : Fallthrough
}
```

The instance this class cannot be cloned and properties cannot be dynamically added to it.


### Methods


#### instance()

```php
static public function instance() : Fallthrough
```

Gets the single instance of the `Fallthrough` class.