<?php namespace JCain\Router\SS\Handlers;

use \JCain\Asserts\LR\AssertArg;
use \JCain\Router\SS\Handler;
use \JCain\Router\SS\HttpStatusException;
use \JCain\Router\SS\Request;


class PhpFileHandler implements Handler {
	private $file;


	public function __construct(array $config) {
		$this->file = AssertArg::isString($config['file'], "\$config['file']");
	}


	//
	// Handler Implementation
	//


	public function handle(Request $request) {
		$file = $this->file;
		if (!is_file($file)) {
			throw new HttpStatusException(404);
		}

		header("Content-Type: text/html; charset=utf-8");
		self::include($file, $request);
	}


	//
	// Static Methods
	//


	static private function include($__FILE__, $request) {
		include $__FILE__;
	}
}