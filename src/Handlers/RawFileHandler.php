<?php namespace JCain\Router\SS\Handlers;

use \JCain\Asserts\LR\AssertArg;
use \JCain\Router\SS\Handler;
use \JCain\Router\SS\HttpStatusException;
use \JCain\Router\SS\Request;


class RawFileHandler implements Handler {
	private $file;
	private $type;


	public function __construct(array $config) {
		$this->file = AssertArg::isString($config['file'], "\$config['file']");
		$this->type = AssertArg::isString($config['type'], "\$config['type']");
	}


	//
	// Handler Implementation
	//


	public function handle(Request $request) {
		$file = $this->file;
		if (!is_file($file)) {
			throw new HttpStatusException(404);
		}

		$type = $this->type;
		if (!$type) {
			$type = 'text/html; charset=utf-8';
		}

		header("Content-Type: $type");
		readfile($file);
	}
}