<?php namespace JCain\Router\SS;

use \JCain\Asserts\LR\Exceptions;


/// Stability: alpha, Since: 0.0
class HttpStatusException extends \Exception {
	public function __construct(int $code, string $message = null) {
		if ($code < 400) {
			throw Exceptions::newInvalidArgument('$code', '400 or greater', $code);
		}

		if ($message === null) {
			switch ($code) {
				case 400: $message = 'Bad Request'; break;
				case 401: $message = 'Unauthorized'; break;
				case 402: $message = 'Payment Required'; break;
				case 403: $message = 'Forbidden'; break;
				case 404: $message = 'Not Found'; break;
				case 405: $message = 'Method Not Allowed'; break;
				case 406: $message = 'Not Acceptable'; break;
				case 407: $message = 'Proxy Authentication Required'; break;
				case 408: $message = 'Request Timeout'; break;
				case 409: $message = 'Conflict'; break;
				case 410: $message = 'Gone'; break;
				case 411: $message = 'Length Required'; break;
				case 412: $message = 'Precondition Failed'; break;
				case 413: $message = 'Payload Too Large'; break;
				case 414: $message = 'URI Too Long'; break;
				case 415: $message = 'Unsupported Media Type'; break;
				case 500: $message = 'Internal Server Error'; break;
				case 501: $message = 'Not Implemented'; break;
				case 502: $message = 'Bad Gateway'; break;
				case 503: $message = 'Service Unavailable'; break;
				case 504: $message = 'Gateway Time-out'; break;
				case 505: $message = 'HTTP Version not supported'; break;
				default:  $message = ''; break;
			}
		}

		parent::__construct($message, $code);
	}
}