<?php namespace JCain\Router\SS\Routers;

use \JCain\Asserts\LR\AssertArg;
use \JCain\Router\SS\HandledException;
use \JCain\Router\SS\HttpStatusException;
use \JCain\Router\SS\Request;
use \JCain\Router\SS\Router;


/// Stability: alpha, Since: 0.9
class BasicRouter implements Router {
	private $outputExceptions;


	public function __construct(array $config = []) {
		$this->outputExceptions = !!$config['outputExceptions'];
	}


	//
	// Methods
	//


	protected function processInstruction($instruction) : void {
		if ($instruction !== null) {
			throw new \LogicException('Unhandled instruction');
		}
	}


	protected function reportException(Request $request, \Throwable $ex) : void {
		if (!headers_sent()) {
			if ($ex instanceof HttpStatusException) {
				http_response_code($ex->getCode());
				header('Content-Type: text/plain; charset=utf-8');
				echo "{$ex->getCode()} {$ex->getMessage()}\n";
			}
			else {
				http_response_code(500);
				header('Content-Type: text/plain; charset=utf-8');
			}
		}

		if ($this->outputExceptions) {
			echo "\n";
			echo $ex;
		}
	}


	//
	// Router Implementation
	//


	public function route(Request $request, $instruction) : void {
		try {
			try {
				$request->evaluate($instruction);
			}
			catch (HandledException $ex) {
				// If the Request was evaluated but did not result in an instruction, throw a 404.
				if ($ex->instruction() === null && $ex->request()->getEvaluated()) {
					throw new HttpStatusException(404);
				}

				$this->processInstruction($ex->instruction());
			}
		}
		catch (\Throwable $ex) {
			$this->reportException($request, $ex);
		}
	}
}