# JCain\Router\SS\Request


## Request interface

_Stability: **alpha**, Since: **0.0**_

```php
interface Request {
  // Methods
  function getVariables() : array
  function setVariables($variables : array) : void
  function patchVariables($variables : array) : void
  function hasVariable($name : mixed) : bool
  function getVariable($name : mixed) : mixed
  function setVariable($name : mixed, $value : mixed) : mixed
  function getEvaluated() : bool
  function setEvaluated($value : boolish) : void
  function evaluate($instruction : mixed) : void
  function fallthrough() : Fallthrough
}
```


### Methods


#### getVariables()

```php
function getVariables() : array
```

Gets all the variables as an associative array.


#### setVariables()

```php
function setVariables($values : array) : void
```

Sets all the variables from an associative array.

This method can be used to remove keys.


#### patchVariables()

```php
function patchVariables($values : array) : void
```

Sets only the variables from those in `$values`.

If the key already exists, its value is replaced. If the key does not exist, it is added.


#### hasVariable()

```php
function hasVariable($name : mixed) : bool
```

Gets whether the variable `$name` exists.


#### getVariable()

```php
function getVariable($name : mixed) : mixed
```

Gets the value for the variable `$name`.


#### setVariable()

```php
function setVariable($name : mixed, $value : mixed) : void
```

Sets the value for the variable `$name` to `$value`.


#### getEvaluated()

```php
function getEvaluated() : bool
```

Gets whether this `Request` is marked as evaluated.


#### setEvaluated()

```php
function setEvaluated($value : boolish) : void
```

Sets whether this `Request` is marked as evaluated.


#### evaluate()

```php
function evaluate($instruction : mixed) : void
```

Evaluates `$instruction` against this `Request` and conditionally throws a [`HandledException`](HandledException.api.md).

The `setEvaluated(true)` method is called before `$instruction` is evaluated.

If `$instruction` is either a function or a [`Handler`](Handler.api.md), then `$instruction` is considered a **handler type** and the **resolved instruction value** is the value returned by `$instruction($request)` or `$instruction->handle($request)`, respectively. Otherwise, `$instruction` is the resolved instruction value. If the resolved instruction value is itself a handler type, then that handler is likewise called, ad infinitum.

The `Request` passed into any handler call is a *duplicate* of this `Request`. How the `Request` is copied (such as by the `clone` expression or by the `new` expression) is determined by the implemention.

If the resolved instruction value equals `fallthrough()`, then this method returns normally. Otherwise, a `HandledException` is thrown with the duplicated `Request` and the resolved instruction value as its values.

If this method is called recursively (by a handler), then a [`LogicException`](https://www.php.net/manual/en/class.logicexception.php) is thrown.


#### fallthrough()

```php
function fallthrough() : Fallthrough
```

Returns the singleton instance of `Fallthrough` (i.e. `Fallthrough::instance()`).