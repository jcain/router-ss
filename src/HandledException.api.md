# JCain\Router\SS\HandledException


## HandledException class

_Stability: **alpha**, Since: **0.0**_

```php
class HandledException extends Exception {
  // Constructor
  public function __construct($request : Request, $instruction : mixed)

  // Methods
  public function request() : Request
  public function instruction() : mixed
}
```


### Constructor


#### __construct()

```php
public function __construct($request : Request, $instruction : mixed)
```


### Methods


#### request()

```php
public function request() : Request
```

Gets the [`Request`](Request.api.md) that was handled.


#### instruction()

```php
public function instruction() : mixed
```

Gets the instruction that resulted from the request being handled.