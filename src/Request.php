<?php namespace JCain\Router\SS;


/// Stability: alpha, Since: 0.0
interface Request {
	function getVariables() : array;


	function setVariables(array $variables) : void;


	function patchVariables(array $variables) : void;


	function hasVariable($name) : bool;


	function getVariable($name);


	function setVariable($name, $value) : void;


	function getEvaluated() : bool;


	function setEvaluated($value) : void;


	function evaluate($instruction) : void;


	function fallthrough() : Fallthrough;
}