<?php namespace JCain\Router\SS;


/// Stability: alpha, Since: 0.9
interface Router {
	function route(Request $request, $instruction) : void;
}