<?php namespace JCain\Router\SS;


/// Stability: alpha, Since: 0.0
interface Handler {
	function handle(Request $request);
}