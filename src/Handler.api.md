# JCain\Router\SS\Handler


## Handler interface

_Stability: **alpha**, Since: **0.0**_

```php
interface Handler {
  // Methods
  function handle($request : Request) : mixed
}
```


### Methods


#### handle()

```php
function handle($request : Request) : mixed
```

Handles a [`Request`](Request.api.md).