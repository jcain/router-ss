<?php namespace JCain\Router\SS\Requests;

use \JCain\Router\SS\HandledException;


/// Stability: alpha, Since: 0.9
class BasicRequest extends RequestBase {
	private $evaluated = false;
	private $evaluating = false;


	//
	// Methods
	//


	public function __clone() {
		$this->evaluated = false;
		$this->evaluating = false;
	}


	//
	// RequestBase Overrides
	//


	public function getEvaluated() : bool {
		return $this->evaluated;
	}


	public function setEvaluated($value) : void {
		$this->evaluated = !!$value;
	}


	public function evaluate($instruction) : void {
		if ($this->evaluating) {
			throw new \LogicException('Recursive call');
		}

		$this->setEvaluated(true);
		$this->evaluating = true;
		try {
			$request = clone $this;

			$instruction = self::resolveInstruction($request, $instruction);
			if ($instruction !== $request->fallthrough()) {
				throw new HandledException($request, $instruction);
			}
		}
		finally {
			$this->evaluating = false;
		}
	}
}