<?php namespace JCain\Router\SS\Requests;

use \JCain\Asserts\LR\AssertArg;
use \JCain\Router\SS\Request;


/// Stability: alpha, Since: 0.9
class TracingRequest extends BasicRequest {
	private $progenitor;
	private $self;


	public function __construct(array $config = []) {
		parent::__construct($config);
		$this->progenitor = AssertArg::isInstanceOrNull($config['progenitor'], Request::class, "\$config['progenitor']");
		$this->self = $this;
	}


	//
	// Methods
	//


	public function generation() : int {
		$depth = 0;
		$curr = $this->progenitor();
		while ($curr) {
			$depth++;
			$curr = $curr->progenitor();
		}
		return $depth;
	}


	public function progenitor() : ?Request {
		return $this->progenitor;
	}


	public function original() : Request {
		do {
			$orig = $curr;
			$curr = $orig->progenitor();
		} while ($curr);
		return $orig;
	}


	public function __clone() {
		parent::__clone();
		$this->progenitor = $this->self;
		$this->self = $this;
	}
}