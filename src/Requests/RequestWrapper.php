<?php namespace JCain\Router\SS\Requests;

use \JCain\Asserts\LR\AssertArg;
use \JCain\Router\SS\Fallthrough;
use \JCain\Router\SS\Request;


/// Stability: alpha, Since: 0.0
class RequestWrapper implements Request {
	private $wrapped;


	public function __construct(Request $wrapped) {
		$this->wrapped = $wrapped;
	}


	//
	// Methods
	//


	public function wrapped() : Request {
		return $this->wrapped;
	}


	//
	// Request Implementation
	//


	public function getVariables() : array {
		return $this->wrapped->getVariables();
	}


	public function setVariables(array $values) : void {
		$this->wrapped->setVariables($values);
	}


	public function patchVariables(array $values) : void {
		$this->wrapped->patchVariables($values);
	}


	public function hasVariable($name) : bool {
		return $this->wrapped->hasVariable($name);
	}


	public function getVariable($name) {
		return $this->wrapped->getVariable($name);
	}


	public function setVariable($name, $value) : void {
		$this->wrapped->setVariable($name, $value);
	}


	public function getEvaluated() : bool {
		return $this->wrapped->evaluated();
	}


	public function setEvaluated($value) : void {
		$this->wrapped->setEvaluated($value);
	}


	public function evaluate($instruction) : void {
		$this->wrapped->evaluate($instruction);
	}


	public function fallthrough() : Fallthrough {
		return $this->wrapped->fallthrough();
	}
}