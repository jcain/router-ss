# JCain\Router\SS\Requests\BasicRequest


## BasicRequest class

_Stability: **alpha**, Since: **0.9**_

```php
class BasicRequest extends RequestBase {
  // Methods
  public function __clone()

  // RequestBase Overrides
  public function getEvaluated() : bool
  public function setEvaluated($value) : void
  public function evaluate($instrunction) : void
}
```


### Methods


#### __clone()

```php
public function __clone()
```


### RequestBase Overrides


#### getEvaluated()

```php
public function getEvaluated() : bool
```

Gets whether this has been evaulated.


#### setEvaluated()

```php
public function setEvaluated($value) : void
```

Sets whether this has been evaulated.


#### evaluate()

```php
public function evaluate($instruction) : void
```

Evaluates this `BasicRequest` against the instruction.