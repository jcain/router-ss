# JCain\Router\SS\Requests\MethodEvaluator


## MethodEvaluator class

_Stability: **alpha**, Since: **0.10**_

```php
class MethodEvaluator extends RequestWrapper {
  // Constructor
  public function __construct($request : Request, $config : array = [])

  // Methods
  public function methodVariable() : string;
  public function methodEquals($method : string, $instruction : mixed) : void
  public function onDelete($strict : boolish, $instruction : mixed) : void
  public function onGet($strict : boolish, $instruction : mixed) : void
  public function onHead($strict : boolish, $instruction : mixed) : void
  public function onOptions($strict : boolish, $instruction : mixed) : void
  public function onPatch($strict : boolish, $instruction : mixed) : void
  public function onPost($strict : boolish, $instruction : mixed) : void
  public function onPut($strict : boolish, $instruction : mixed) : void
}
```


### Methods


#### methodVariable()

```php
public function methodVariable() : string
```


#### methodEquals()

```php
public function methodEquals($method : string, $instruction : mixed) : void
```


#### onDelete()

```php
public function onDelete($strict : boolish, $instruction : mixed) : void
```


#### onGet()

```php
public function onGet($strict : boolish, $instruction : mixed) : void
```


#### onHead()

```php
public function onHead($strict : boolish, $instruction : mixed) : void
```


#### onOptions()

```php
public function onOptions($strict : boolish, $instruction : mixed) : void
```


#### onPatch()

```php
public function onPatch($strict : boolish, $instruction : mixed) : void
```


#### onPost()

```php
public function onPost($strict : boolish, $instruction : mixed) : void
```


#### onPut()

```php
public function onPut($strict : boolish, $instruction : mixed) : void
```