<?php namespace JCain\Router\SS\Requests;

use \JCain\Asserts\LR\AssertArg;
use \JCain\Router\SS\Fallthrough;
use \JCain\Router\SS\Handler;
use \JCain\Router\SS\Request;


/// Stability: alpha, Since: 0.9
abstract class RequestBase implements Request {
	private $variables;


	public function __construct(array $config = []) {
		$this->variables = AssertArg::isArrayOrNull($config['variables'], "\$config['variables']") ?: [];
	}


	//
	// Request Implementation
	//


	public function getVariables() : array {
		return $this->variables;
	}


	public function setVariables(array $variables) : void {
		$this->variables = $variables;
	}


	public function patchVariables(array $variables) : void {
		$this->variables = $variables + $this->variables;
	}


	public function hasVariable($name) : bool {
		return array_key_exists($name, $this->variables);
	}


	public function getVariable($name) {
		return $this->variables[$name];
	}


	public function setVariable($name, $value) : void {
		$this->variables[$name] = $value;
	}


	public function fallthrough() : Fallthrough {
		return Fallthrough::instance();
	}


	//
	// Static Methods
	//


	static public function resolveInstruction(Request $request, $instruction) {
		while ($instruction !== null) {
			if ($instruction instanceof \Closure) {
				$instruction = $instruction($request);
			}
			else if ($instruction instanceof Handler) {
				$instruction = $instruction->handle($request);
			}
			else {
				break;
			}
		}

		return $instruction;
	}


	static public function parseRequestInfo(array $options = []) : array {
		$method = $options['method'];
		if (!is_string($method)) {
			$method = $_SERVER['REQUEST_METHOD'];
		}

		$uri = $options['uri'];
		if (!is_string($uri)) {
			$uri = $_SERVER['REQUEST_URI'];
		}

		[ $pathPart, $queryPart ] = explode('?', $uri, 2);

		$path = $options['path'];
		if (!is_string($path)) {
			$path = substr($pathPart, 1);
		}

		$query = $options['query'];
		if (!is_string($query)) {
			$query = $queryPart ?? '';
		}

		return [
			'method' => $method,
			'path' => $path,
			'query' => $query,
		];
	}
}