# JCain\Router\SS\Requests\StringEvaluator


## StringEvaluator interface

_Stability: **alpha**, Since: **0.10**_

```php
class StringEvaluator extends RequestWrapper {
  // Constructor
  public function __construct($wrapped : Request, $variable : string)

  // Methods
  public function beginsWith($prefix : string, $instruction : mixed) : void
  public function endsWith($suffix : string, $instruction : mixed) : void
  public function equals($str : string, $instruction : mixed) : void
  public function matches($regex : string, $instruction : mixed) : void
}
```


### Constructor


#### __construct()

```php
public function __construct($wrapped : Request, $variable : string)
```


### Methods


#### beginsWith()

```php
public function beginsWith($prefix : string, $instruction : mixed) : void
```


#### endsWith()

```php
public function endsWith($suffix : string, $instruction : mixed) : void
```


#### equals()

```php
public function equals($str : string, $instruction : mixed) : void
```


#### matches()

```php
public function matches($regex : string, $instruction : mixed) : void
```