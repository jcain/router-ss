<?php namespace JCain\Router\SS\Requests;

use \JCain\Asserts\LR\AssertArg;
use \JCain\Router\SS\Request;
use \JCain\Router\SS\HttpStatusException;


/// Stability: alpha, Since 0.10
class MethodEvaluator extends RequestWrapper {
	public const METHOD_DELETE = 'DELETE';
	public const METHOD_GET = 'GET';
	public const METHOD_HEAD = 'HEAD';
	public const METHOD_OPTIONS = 'OPTIONS';
	public const METHOD_PATCH = 'PATCH';
	public const METHOD_POST = 'POST';
	public const METHOD_PUT = 'PUT';

	private $variable;
	private $fallthroughWhenUnmatched;


	public function __construct(Request $request, string $variable, array $options = []) {
		parent::__construct($request);
		$this->variable = $variable;
		$this->fallthroughWhenUnmatched = !!$options['fallthroughWhenUnmatched'];
	}


	//
	// Methods
	//


	public function variable() : string {
		return $this->variable;
	}


	public function equals($method, $instruction) : void {
		$this->evaluate(function ($request) use ($method, $instruction) {
			$value = $request->getVariable($this->variable);

			if (is_array($method)) {
				$matched = in_array($value, $method, true);
				if (!$matched) {
					return ($this->fallthroughWhenUnmatched ? $request->fallthrough() : $this->unmatched());
				}
			}
			else if ($value !== $method) {
				return ($this->fallthroughWhenUnmatched ? $request->fallthrough() : $this->unmatched());
			}

			return $instruction;
		});
	}


	public function onDelete($instruction) : void {
		$this->equals(self::METHOD_DELETE, $instruction);
	}


	public function onGet($instruction) : void {
		$this->equals(self::METHOD_GET, $instruction);
	}


	public function onGetOrHead($instruction) : void {
		$this->equals([ self::METHOD_GET, self::METHOD_HEAD ], $instruction);
	}


	public function onHead($instruction) : void {
		$this->equals(self::METHOD_HEAD, $instruction);
	}


	public function onOptions($instruction) : void {
		$this->equals(self::METHOD_OPTIONS, $instruction);
	}


	public function onPatch($instruction) : void {
		$this->equals(self::METHOD_PATCH, $instruction);
	}


	public function onPost($instruction) : void {
		$this->equals(self::METHOD_POST, $instruction);
	}


	public function onPut($instruction) : void {
		$this->equals(self::METHOD_PUT, $instruction);
	}


	public function unmatched() {
		throw new HttpStatusException(405);
	}
}