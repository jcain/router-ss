<?php namespace JCain\Router\SS\Requests;

use \JCain\Asserts\LR\AssertArg;
use \JCain\Router\SS\Request;


/// Stability: alpha, Since 0.10
class StringEvaluator extends RequestWrapper {
	private $variable;


	public function __construct(Request $request, string $variable) {
		parent::__construct($request);
		$this->variable = $variable;
	}


	//
	// Methods
	//


	public function variable() : string {
		return $this->variable;
	}


	public function beginsWith(string $prefix, $instruction) : void {
		$this->evaluate(function ($request) use ($prefix, $instruction) {
			$length = strlen($prefix);
			$value = $request->getVariable($this->variable);
			if (substr($value, 0, $length) !== $prefix) {
				return $request->fallthrough();
			}

			$request->setVariable($this->variable, substr($value, $length));
			return $instruction;
		});
	}


	public function endsWith(string $suffix, $instruction) : void {
		$this->evaluate(function ($request) use ($suffix, $instruction) {
			$value = $request->getVariable($this->variable);
			if (substr($value, -strlen($suffix)) !== $suffix) {
				return $request->fallthrough();
			}

			return $instruction;
		});
	}


	public function equals(string $str, $instruction) : void {
		$this->evaluate(function ($request) use ($str, $instruction) {
			$value = $request->getVariable($this->variable);
			if ($value !== $str) {
				return $request->fallthrough();
			}

			$request->setVariable($this->variable, '');
			return $instruction;
		});
	}


	public function matches(string $regex, $instruction) : void {
		$this->evaluate(function ($request) use ($regex, $instruction) {
			$value = $request->getVariable($this->variable);
			$regex = '/' . preg_replace('/\//', '\/', $regex) . '/u';

			$matches = [];
			$matched = preg_match($regex, $value, $matches, PREG_OFFSET_CAPTURE);
			if (!$matched) {
				return $request->fallthrough();
			}

			$request->setVariable($this->variable, substr($value, $matches[0][1] + strlen($matches[0][0])));
			foreach ($matches as $k => $v) {
				$request->setVariable($k, $v[0]);
			}

			return $instruction;
		});
	}
}