<?php namespace JCain\Router\SS;


/// Stability: alpha, Since: 0.0
class HandledException extends \Exception {
	private $request;
	private $instruction;


	public function __construct(Request $request, $instruction) {
		$this->request = $request;
		$this->instruction = $instruction;
	}


	public function request() : Request {
		return $this->request;
	}


	public function instruction() {
		return $this->instruction;
	}
}