# JCain\Router\SS\Router


## Router interface

_Stability: **alpha**, Since: **0.9**_

```php
interface Router {
  // Methods
  function router($request : Request, $instruction :  mixed) : void
}
```


### Methods


#### router()

```php
function route($request : Request, $instruction : mixed) : void
```

Routes a [`Request`](Request.api.md).

Implementations will call `$request->evaluate($instruction)` and handle exceptions (especially [`HandledException`](HandledException.api.md)) that are thrown by it.