# Router SS

Router SS is an HTTP router micro-library for PHP 7.2+.


## Installation

Router SS requires PHP 7.2 or greater.

```batchfile
composer require jcain/router-ss
```


## Usage

Create a `router.php` file with the following contents:

```php
<?php
require_once __DIR__ . '/vendor/autoload.php';

use \JCain\Router\SS\Requests\BasicRequest;
use \JCain\Router\SS\Requests\StringEvaluator;
use \JCain\Router\SS\Routers\BasicRouter;

$request = new BasicRequest([
  'variables' => BasicRequest::parseRequestInfo(),
]);

$router = new BasicRouter([ 'outputExceptions' => true ]);
$router->route($request, function ($r) {
  $path = new StringEvaluator($r, 'path');

  $path->equals('', function ($r) {
    echo '<a href="hello-world">Click here to greet the world<a><br />';
    echo 'or<br />';
    echo '<a href="hello-universe">Click here to greet the universe<a><br />';
  });

  $path->matches('^hello-(?<name>[^/]+)$', function ($r) {
    $name = ucwords(urldecode($r->getVariable('name')));
    echo "Hello, $name!";
  });
});
```

Run the built-in PHP server:

```batchfile
php -S localhost:8080 router.php
```

Then visit http://localhost:8080/.


## Component stability statuses

| Component                                                               | Stability | Since |
|:------------------------------------------------------------------------|:---------:|:-----:|
| [Fallthrough](src/Fallthrough.api.md)                                   | alpha     | 0.9   |
| [HandledException](src/HandledException.api.md)                         | alpha     | 0.0   |
| [Handler](src/Handler.api.md)                                           | alpha     | 0.0   |
| [HttpStatusException](src/HttpStatusException.api.md)                   | alpha     | 0.0   |
| [Request](src/Request.api.md)                                           | alpha     | 0.0   |
| [RequestProvider](src/RequestProvider.api.md)                           | alpha     | 0.9   |
| [Router](src/Router.api.md)                                             | alpha     | 0.9   |
| [Handlers/PhpFileHandler](src/Handlers/PhpFileHandler.api.md)           | alpha     | 0.7   |
| [Handlers/RawFileHandler](src/Handlers/RawFileHandler.api.md)           | alpha     | 0.7   |
| [Matchers/BasicMatchers](src/Matchers/BasicMatchers.api.md)             | alpha     | 0.9   |
| [Matchers/MethodMatchers](src/Matchers/MethodMatchers.api.md)           | alpha     | 0.9   |
| [Matchers/MethodMatchersTrait](src/Matchers/MethodMatchersTrait.api.md) | alpha     | 0.7   |
| [Matchers/PathMatchers](src/Matchers/PathMatchers.api.md)               | alpha     | 0.9   |
| [Matchers/PathMatchersTrait](src/Matchers/PathMatchersTrait.api.md)     | alpha     | 0.7   |
| [Requests/BasicRequest](src/Requests/BasicRequest.api.md)               | alpha     | 0.9   |
| [Requests/RequestBase](src/Requests/RequestBase.api.md)                 | alpha     | 0.9   |
| [Requests/RequestWrapper](src/Requests/RequestWrapper.api.md)           | alpha     | 0.9   |
| [Requests/TracingRequest](src/Requests/TracingRequest.api.md)           | alpha     | 0.9   |
| [Routers/BasicRouter](src/Routers/BasicRouter.api.md)                   | alpha     | 0.9   |

The **Stability** column indicates the component's stability status, and the **Since** column indicates the package's major version when the component first achieved that stability.

Each component and its members has a **stability status** indicating how stable the interface and implementation is to depend on in production. The stability may be one of the following:

* **alpha**: The interface and implementation are unstable and may change significantly.
* **beta**: The interface is stable but its implementation is not sufficiently tested.
* **omega**: The interface and implementation are stable and considered ready for production use.

A component's stability status is the same as the highest stability status of its members. Once a member's stability is raised, it will not be reduced.


## License

Router SS is licensed under the MIT license. See the [LICENSE](LICENSE.md) file for more information.